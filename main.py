from fastapi import FastAPI
from fastapi.responses import HTMLResponse
from config.database import Engine,Base
from middlewares.error_handler import ErrorHandler
from routers.movie import movie_router
from routers.user import user_router



app = FastAPI()
app.title="Mi Aplicación con FastAPI"
app.version="0.0.1"

app.add_middleware(ErrorHandler)
app.include_router(movie_router)
app.include_router(user_router)

Base.metadata.create_all(bind=Engine)






movies = [
    {
        'id': 1,
        'title': 'Avatar',
        'overview': "En un exuberante planeta llamado Pandora viven los Na'vi, seres que ...",
        'year': '2009',
        'rating': 7.8,
        'category': 'Acción'    
    },
     {
        'id': 2,
        'title': 'Avatar2',
        'overview': "En un 2 exuberante planeta llamado Pandora viven los Na'vi, seres que ...",
        'year': '2020',
        'rating': 9,
        'category': 'Acción'    
    }  
]

@app.get('/',tags=['Home'])
def message():
    return HTMLResponse('<h1>Hello world!</h1>')


