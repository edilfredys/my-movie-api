import os
from sqlalchemy import create_engine
from sqlalchemy.orm.session import sessionmaker
from sqlalchemy.ext.declarative import declarative_base


Sqlite_file_name="../database.sqlite"
Base_dir=os.path.dirname(os.path.realpath(__file__))

Database_url=f"sqlite:///{os.path.join(Base_dir,Sqlite_file_name)}"

Engine=create_engine(Database_url,echo=True)

Session=sessionmaker(bind=Engine)

Base=declarative_base()